import React from "react";
import "./App.css";
import logoBrazil from "./images/brazil-flag-icon.png";
import logoSpain from "./images/spain-flag-icon.png";
import Game from "./components/game";
import KickSoundEffect from "./sounds/bounces.mp3";
import GoalSoundEffect from "./sounds/diving.mp3";

class App extends React.Component {
  state = {
    score1: 0,
    score2: 0,
    count1: 0,
    count2: 0,
    statistic1: 0,
    statistic2: 0,
    show1: false,
    show2: false,
  };

  countAudio = new Audio(KickSoundEffect); //faz parte do javascript
  goalAudio = new Audio(GoalSoundEffect);
  playSound = (x) => {
    x.play();
  };

  RandomNumber = () => {
    let resul = Math.ceil(Math.random() * (1 - -1) + -1);
    if (resul === 1) {
      this.playSound(this.goalAudio);
    } else {
      this.playSound(this.countAudio);
    }
    return resul;
  };

  player1Button = () => {
    const { score1, count1 } = this.state;
    const x = this.RandomNumber();

    this.setState({
      score1: score1 + x,
      count1: count1 + 1,
      show1: true,
      show2: false,
      statistic1: (((score1 + x) / (count1 + 1)) * 100).toFixed(1),
    });
  };

  player2Button = () => {
    const { score2, count2 } = this.state;
    const y = this.RandomNumber();

    this.setState({
      score2: score2 + y,
      count2: count2 + 1,
      show1: false,
      show2: true,
      statistic2: (((score2 + y) / (count2 + 1)) * 100).toFixed(1),
    });
  };

  render() {
    const {
      score1,
      score2,
      count1,
      count2,
      show1,
      show2,
      statistic1,
      statistic2,
    } = this.state;

    return (
      <div className="App">
        <Game
          logo1={logoBrazil}
          button1={this.player1Button}
          score1={score1}
          counter1={count1}
          show1={show1}
          statistic1={statistic1}
          logo2={logoSpain}
          button2={this.player2Button}
          score2={score2}
          counter2={count2}
          show2={show2}
          statistic2={statistic2}
        />
      </div>
    );
  }
}

export default App;
