import React from "react";

const Team = (props) => {
  return (
    <div>
      <img src={props.src} alt={props.alt} />
      <h1>{props.name}</h1>
      <button onClick={props.onClick}>Kick!</button>
      <h3>Total score: {props.score}</h3>
      <h4>Total kicks: {props.count}</h4>
    </div>
  );
};

export default Team;
