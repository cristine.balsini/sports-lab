import React from "react";
import Team from "../team";

class Game extends React.Component {
  render() {
    return (
      <>
        <div className="App-header">
          <Team
            src={this.props.logo1}
            alt={"logo"}
            name={"Player 1"}
            onClick={this.props.button1}
            score={this.props.score1}
            count={this.props.counter1}
          />
          {this.props.show1 && <div>Performance: {this.props.statistic1}%</div>}
        </div>
        <h1 className="stadiumTitle">Welcome to Fardo Stadium!</h1>
        <div className="App-header">
          <Team
            src={this.props.logo2}
            alt={"logo"}
            name={"Player 2"}
            onClick={this.props.button2}
            score={this.props.score2}
            count={this.props.counter2}
          />
          {this.props.show2 && <div>Performance: {this.props.statistic2}%</div>}
        </div>
      </>
    );
  }
}

export default Game;
